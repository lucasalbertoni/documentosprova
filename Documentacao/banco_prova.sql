-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 03/04/2017 às 02h47min
-- Versão do Servidor: 5.0.45
-- Versão do PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `banco_prova`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE IF NOT EXISTS `endereco` (
  `id` int(12) NOT NULL auto_increment,
  `cep` varchar(8) NOT NULL,
  `logradouro` varchar(100) NOT NULL,
  `bairro` varchar(40) default NULL,
  `numero` varchar(40) NOT NULL,
  `complemento` varchar(50) default NULL,
  `cidade` varchar(40) NOT NULL,
  `estado` char(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id`, `cep`, `logradouro`, `bairro`, `numero`, `complemento`, `cidade`, `estado`) VALUES
(3, '01331020', 'Rua Doutor Seng', 'Bela Vista', '22', NULL, 'São Paulo', 'SP'),
(4, '01331020', 'Rua Doutor Seng', 'Bela Vista', '22', 'S/ número', 'São Paulo', 'SP'),
(6, '11740000', 'Avenida Rui Barbosa', 'Centro', '754', NULL, 'Itanhaém', 'SP'),
(7, '04855990', 'Rua Vitória nº 17-B - Parque São Miguel (Zona Sul)', 'Centro', '754', NULL, 'São Paulo', 'SP');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
